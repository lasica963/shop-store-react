import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';
import {compose} from 'recompose';
import {ProductConsumer} from "../context";

const Navbar = () => (
  <div className="row bg--blue">
    <nav className='container auto flex justify-between padt25 padb25'>
      <Link to='/'>
        Product
      </Link>
      <Link to='/card'>
        <button className='btn padt15 padb15 padr15 padl15 bg--white rel'>
          <i className="fas fa-shopping-cart"></i>
          <span className='abs top-10 right-10'>
              <ProductConsumer>
              {
                value => {
                  const {cart} = value;
                  return (
                    <div>
                      {cart.length}
                    </div>
                  )
                }
              }
            </ProductConsumer>
          </span>
        </button>
      </Link>

    </nav>
  </div>
);

export default compose(

)(Navbar);