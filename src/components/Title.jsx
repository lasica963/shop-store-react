import React from 'react';
import {compose} from 'recompose';

const Title = ({name, title}) => (
    <div className='row mt50'>
            <h2>{name}</h2>
            <h3>{title}</h3>
    </div>
);

export default compose()(Title);