import React from 'react'
import PayPalButton from './PayPallButton';

export default function CartTotal({value,history}) {

    const {cartSubTotal, cartTax, cartTotal, clearCart} = value;
    return (
        <div className='container auto flex flex-wrap justify-end'>
            <div>
                <button className='btn btn--blue padt10 padb10 padr25 padl25' onClick={() => clearCart()}>
                    Clear Cart
                </button>
                <p>SubTotal: {cartSubTotal}</p>
                <p>Tax: {cartTax}</p>
                <p>Total: {cartTotal}</p>
            </div>
            <div className="row flex justify-end mt30">
                <PayPalButton total={cartTotal} clearCart={clearCart} history={history}/>
            </div>
        </div>
    )
}