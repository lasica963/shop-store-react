import React from 'react'

export default function CartList({item, value}) {
    const {id, title, img, price, total, count} = item;
    const {increment, decrement, removeItem} = value;
    return (
        <div className="row flex mt30">
            <div className="col-2">
                <img src={img} alt="" className="cart-img-item"/>
            </div>
            <div className="col-2">
                {title}
            </div>
            <div className="col-2">
                {price}
            </div>
            <div className="col-2">
                <div className="row flex">
                    <span className="btn btn--quantitiy" onClick={() => decrement(id)}>-</span>
                    <span className='ml10 padt10 mr10'>{count}</span>
                    <span className="btn btn--quantitiy" onClick={() => increment(id)}>+</span>
                </div>
            </div>
            <div className="col-2">
                <div onClick={() => removeItem(id)}>
                    <i className="fas fa-trash"></i>
                </div>
            </div>
            <div className="col-2">
                {total}
            </div>
        </div>
    )
}