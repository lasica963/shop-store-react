import React from 'react'

export default function EmptyCart() {
    return (
        <div className="container auto">
            <h1 className='txt-center'>Your cart is empty</h1>
        </div>
    )
}