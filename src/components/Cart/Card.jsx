import React, {Component} from 'react';
import CartColumns from '../Cart/CartColumns';
import EmptyCart from '../Cart/EmptyCart';
import {ProductConsumer} from "../../context";
import CartList from '../Cart/CartList';
import CartTotal from '../Cart/CartTotal';

export default class Card extends Component {
    render() {
        return (
            <div className='row flex flex-wrap justify-center'>
                <ProductConsumer>
                    {value => {
                        const {cart} = value;

                        if (cart.length > 0) {
                            return (
                                <React.Fragment>
                                    <h1 className='row txt-center mt30 mb50'>Your cart</h1>
                                    <CartColumns/>
                                    <CartList value={value}/>
                                    <CartTotal value={value} history={this.props.history}/>
                                </React.Fragment>
                            )
                        } else {
                            return <EmptyCart/>
                        }
                    }}
                </ProductConsumer>
            </div>
        )
    }
}