import React from 'react';
import {compose} from 'recompose';
import Title from "../components/Title";
import {ProductConsumer} from "../context";
import Product from "./Product";
import PropTypes from 'prop-types';

const ProductList = () => (

    <React.Fragment>
        <div className="container auto padt5">
            <div className="row flex txt-center">
                <Title name="Our" title='products'/>
            </div>
            <div className="row flex flex-wrap padb100">
                <ProductConsumer>
                    {(value) => {
                        return value.products.map((product) => {
                            return <Product key={product.id} product={product}/>
                        })
                    }}
                </ProductConsumer>
            </div>
        </div>
    </React.Fragment>

);

Product.propTypes = {
    product: PropTypes.shape({
        id: PropTypes.number,
        img: PropTypes.string,
        title: PropTypes.string,
        price: PropTypes.number,
        inCart: PropTypes.bool
    }).isRequired
};

export default compose()(ProductList);