import React, {Fragment} from 'react';
import {compose, withProps} from 'recompose';
import {ProductConsumer} from "../context";
import {Link} from "react-router-dom";


const Details = () => (
    <div className='container auto mt100 flex flex-wrap'>
        <ProductConsumer>
            {
                value => {
                    const {id, company, img, info, title, price, inCart} = value.detailProduct;
                    return (
                        <Fragment>
                            <div className='col-5'>
                                <img src={img} alt=""/>
                            </div>
                            <div className="col-6">
                                <h1 className='mb15'>{title}</h1>
                                <h6>Made by: <span className='uppercase'>{company}</span></h6>
                                <p>
                                    {info}
                                </p>
                                <span className='bl mt20'>
                                    Price: {price} $
                                </span>
                                <div className="row mt20 ">
                                    <Link to='/'>
                                        <button className='btn btn--blue padt15 padb15 padr25 padl25'>
                                            Back
                                        </button>
                                    </Link>
                                    <button className='btn btn--yellow padt15 padb15 padr25 padl25 ml20'
                                            disabled={inCart ? true : false}
                                            onClick={() => {
                                                value.addToCart(id);
                                            }}
                                    >
                                        {inCart ? 'inCart' : 'Add in cart'}
                                    </button>
                                </div>
                            </div>

                        </Fragment>
                    )
                }
            }
        </ProductConsumer>
    </div>
);

export default compose(


)(Details);