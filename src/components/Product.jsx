import React from 'react';
import {ProductConsumer} from "../context";
import {Link} from "react-router-dom";
import {compose, withProps} from 'recompose';

const Product = (props) => (
    <ProductConsumer>
        {(value) => (
            <div className="col-3 padr15 padl15 mt30" onClick={() => {
                value.handleDetail(props.product.id)
            }}>
                <div className="row product-box rel">
                    <Link to='/details'>
                        <img src={props.product.img} alt=""/>
                    </Link>
                    <button className='btn btn--blue padt20 padb20 padr20 padl20 abs bottom0 right0'
                            disabled={props.product.inCart ? true : false}
                            onClick={() => {
                                value.addToCart(props.product.id);
                            }}
                    >
                        {props.product.inCart ? (<span>{''}inCart</span>) : (<i className="fas fa-cart-plus"></i>)}
                    </button>
                    <div className="row mt20">
                        <h6>{props.product.title}</h6>
                        <span>{props.product.price} $</span>
                    </div>

                </div>
            </div>
        )}
    </ProductConsumer>
);
export default compose(
    withProps((props) => {
    })
)(Product);