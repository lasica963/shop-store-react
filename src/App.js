import React from 'react';
import {Switch, Route} from 'react-router-dom';

import './sass/core.scss';

//Components
import Navbar from './components/Navbar';
import Card from './components/Cart/Card';
import Details from './components/Details';
import Default from './components/Default';
import ProductList from './components/ProductList';


const App = () => (
    <React.Fragment>
        <Navbar/>
        <Switch>
            <Route exact path='/' component={ProductList}/>
            <Route path='/details' component={Details}/>
            <Route path='/card' component={Card}/>
            <Route component={Default}/>
        </Switch>
    </React.Fragment>
);


export default () => (
    <App/>
)